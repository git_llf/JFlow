package bp.wf.template;

/** 
 手工启动子流程属性
*/
public class SubFlowHandAttr extends SubFlowAttr
{
    public static final String SubFlowLab = "SubFlowLab";

    public static final String SubFlowStartModel="SubFlowStartModel";
    public static final String SubFlowShowModel="SubFlowShowModel" ;
}