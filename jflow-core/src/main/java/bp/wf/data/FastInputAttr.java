package bp.wf.data;
import bp.en.EntityNoNameAttr;

/** 
 常用语属性
*/
public class FastInputAttr extends EntityNoNameAttr
{
	
	public static final String CfgKey = "CfgKey";
	public static final String EnsName = "EnsName";
	/** 
	 类型
	*/
	public static final String AttrKey = "AttrKey";
	/** 
	 Vals
	*/
	public static final String Vals = "Vals";
	/** 
	 流程编号
	*/
	public static final String FK_Emp = "FK_Emp";
	/** 
	 顺序号
	*/
	public static final String Idx = "Idx";
}