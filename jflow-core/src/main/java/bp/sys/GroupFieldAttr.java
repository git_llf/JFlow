package bp.sys;
import bp.en.EntityOIDAttr;

/** 
 GroupField
*/
public class GroupFieldAttr extends EntityOIDAttr
{
	/** 
	 表单ID
	*/
	public static final String FrmID = "FrmID";
	/** 
	 Lab
	*/
	public static final String Lab = "Lab";
	/** 
	 Idx
	*/
	public static final String Idx = "Idx";
	/** 
	 控件类型
	*/
	public static final String CtrlType = "CtrlType";
	/** 
	 控件ID
	*/
	public static final String CtrlID = "CtrlID";
	/** 
	 PC端是否折叠显示？
	*/
	public static final String IsZDPC = "IsZDPC";
	/** 
	 手机端是否折叠显示？
	*/
	public static final String IsZDMobile = "IsZDMobile";
	/**
	 * 分组显示的模式
	 */
	public static final String ShowType="ShowType";
}