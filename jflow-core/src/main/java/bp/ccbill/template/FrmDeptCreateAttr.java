package bp.ccbill.template;

import bp.da.*;
import bp.en.*;
import bp.wf.port.*;
import bp.ccbill.*;
import java.util.*;

/** 
 单据可创建的部门属性	  
*/
public class FrmDeptCreateAttr
{
	/** 
	 单据
	*/
	public static final String FrmID = "FrmID";
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
}